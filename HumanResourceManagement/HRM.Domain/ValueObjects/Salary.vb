﻿Imports HRM.Domain.Helpers

Namespace ValueObjects

    Public Class Salary
        Inherits AbstractValueObjectBase(Of Salary)

        Private Const Unit As String = "$"

        Public ReadOnly Property Value As Decimal?
        Public ReadOnly Property DisplayValue As String
            Get
                If Not Value.HasValue Then Return "-"
                Dim retVal As Decimal = Value
                Return retVal.RoundString(2) & Unit
            End Get
        End Property

        Public Sub New(value As Decimal?)
            Me.Value = value
        End Sub

        Protected Overrides Function EqualsCore(other As Salary) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace