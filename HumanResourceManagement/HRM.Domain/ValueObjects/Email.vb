﻿Namespace ValueObjects

    Public Class Email
        Inherits AbstractValueObjectBase(Of Email)

        Private Const DomainName As String = "oracle.com"

        Public ReadOnly Property Value As String
        Public ReadOnly Property DisplayValue As String
            Get
                Return Value & "@" & DomainName
            End Get
        End Property

        Public Sub New(value As String)
            Me.Value = value
        End Sub

        Protected Overrides Function EqualsCore(other As Email) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace