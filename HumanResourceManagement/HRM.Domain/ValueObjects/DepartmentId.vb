﻿Namespace ValueObjects

    Public Class DepartmentId
        Inherits AbstractValueObjectBase(Of DepartmentId)

        Public ReadOnly Property Value As Integer?
        Public ReadOnly Property DisplayValue As String
            Get
                If Not Value.HasValue Then Return "-"
                Return Value.ToString()
            End Get
        End Property

        Public Sub New(value As Integer?)
            Me.Value = value
        End Sub

        Protected Overrides Function EqualsCore(other As DepartmentId) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace