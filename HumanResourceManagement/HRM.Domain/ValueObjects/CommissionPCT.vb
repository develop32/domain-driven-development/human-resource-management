﻿Imports HRM.Domain.Helpers

Namespace ValueObjects

    Public Class CommissionPCT
        Inherits AbstractValueObjectBase(Of CommissionPCT)

        Public ReadOnly Property Value As Decimal?

        Public ReadOnly Property DisplayValue As String
            Get
                If Not Value.HasValue Then Return "-"
                Dim retVal As Decimal = Value
                Return retVal.RoundString(2)
            End Get
        End Property

        Public Sub New(value As Nullable(Of Decimal))
            Me.Value = value
        End Sub

        Protected Overrides Function EqualsCore(other As CommissionPCT) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace