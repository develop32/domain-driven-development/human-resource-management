﻿Namespace ValueObjects
    ''' <summary>
    ''' ValueObject抽象クラス
    ''' </summary>
    ''' <typeparam name="T">自分自身のクラス</typeparam>
    Public MustInherit Class AbstractValueObjectBase(Of T As AbstractValueObjectBase(Of T))
#Region "メソッド"
        ''' <summary>
        ''' 指定のオブジェクトと等しいかを返します。
        ''' </summary>
        ''' <param name="obj">比較対象オブジェクト</param>
        ''' <returns>True：等しい、False：等しくない</returns>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Dim vo = TryCast(obj, T)

            If vo Is Nothing Then
                Return False
            End If

            Return EqualsCore(vo)
        End Function

        ''' <summary>
        ''' 指定のオブジェクトと等しいかを返します。
        ''' </summary>
        ''' <param name="vo1">比較対象オブジェクト1</param>
        ''' <param name="vo2">比較対象オブジェクト1</param>
        ''' <returns>True：等しい、False：等しくない</returns>
        Public Shared Operator =(ByVal vo1 As AbstractValueObjectBase(Of T), ByVal vo2 As AbstractValueObjectBase(Of T)) As Boolean
            Return Equals(vo1, vo2)
        End Operator

        ''' <summary>
        ''' 指定のオブジェクトが等しくないかを返します。
        ''' </summary>
        ''' <param name="vo1">比較対象オブジェクト1</param>
        ''' <param name="vo2">比較対象オブジェクト2</param>
        ''' <returns>True：等しくない、False：等しい</returns>
        Public Shared Operator <>(ByVal vo1 As AbstractValueObjectBase(Of T), ByVal vo2 As AbstractValueObjectBase(Of T)) As Boolean
            Return Not Equals(vo1, vo2)
        End Operator

        ''' <summary>
        ''' 指定のオブジェクトと等しいかを返します。
        ''' </summary>
        ''' <param name="other">比較対象オブジェクト</param>
        ''' <returns>True：等しい、False：等しくない</returns>
        Protected MustOverride Function EqualsCore(ByVal other As T) As Boolean

        ''' <summary>
        ''' 文字列へ変換します。
        ''' </summary>
        ''' <returns>変換した文字列</returns>
        Public Overrides Function ToString() As String
            Return MyBase.ToString()
        End Function

        ''' <summary>
        ''' ハッシュコードを取得します。
        ''' </summary>
        ''' <returns>ハッシュコード</returns>
        Public Overrides Function GetHashCode() As Integer
            Return MyBase.GetHashCode()
        End Function
#End Region
    End Class

End Namespace