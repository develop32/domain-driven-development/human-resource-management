﻿Namespace Exceptions
    ''' <summary>
    ''' 例外抽象クラス
    ''' </summary>
    Public MustInherit Class AbstractExceptionBase
        Inherits Exception
#Region "プロパティ"
        ''' <summary>種類</summary>
        ''' <returns>種類</returns>
        Public MustOverride ReadOnly Property Kind As ExceptionKind
#End Region

#Region "コンストラクタ"
        ''' <summary>コンストラクタ</summary>
        ''' <param name="message">例外メッセージ</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>コンストラクタ</summary>
        ''' <param name="message">例外メッセージ</param>
        ''' <param name="exception">エクセプション</param>
        Public Sub New(ByVal message As String, ByVal exception As Exception)
            MyBase.New(message, exception)
        End Sub
#End Region

#Region "列挙型"
        Public Enum ExceptionKind
            ''' <summary>情報</summary>
            Info
            ''' <summary>警告</summary>
            Warning
            ''' <summary>エラー</summary>
            [Error]
        End Enum
#End Region

    End Class

End Namespace