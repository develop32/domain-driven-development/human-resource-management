﻿Namespace Exceptions
    ''' <summary>入力例外クラス</summary>
    Public Class InputException
        Inherits AbstractExceptionBase

#Region "プロパティ"

        Public Overrides ReadOnly Property Kind As ExceptionKind
            Get
                Return ExceptionKind.Warning
            End Get
        End Property
#End Region

#Region "コンストラクタ"
        ''' <summary>コンストラクタ</summary>
        ''' <param name="message">例外メッセージ</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>コンストラクタ</summary>
        ''' <param name="message">例外メッセージ</param>
        ''' <param name="exception">エクセプション</param>
        Public Sub New(ByVal message As String, ByVal exception As Exception)
            MyBase.New(message, exception)
        End Sub
#End Region
    End Class

End Namespace