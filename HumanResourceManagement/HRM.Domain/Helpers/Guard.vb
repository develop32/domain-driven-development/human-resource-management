﻿Imports HRM.Domain.Exceptions

Namespace Helpers
    ''' <summary>ガードクラス</summary>
    Public Class Guard
#Region "メソッド"
        ''' <summary>
        ''' 指定の文字列が空文字の場合入力エラーにします。
        ''' </summary>
        ''' <param name="text">対象文字列</param>
        ''' <param name="message">エラー時のメッセージ</param>
        Public Shared Sub IsStringEmpty(ByVal text As String, ByVal message As String)
            If text = String.Empty Then
                Throw New InputException(message)
            End If
        End Sub

        ''' <summary>
        ''' 指定のオブジェクトがNothingの場合指定のエラーにします。
        ''' </summary>
        ''' <param name="o">オブジェクト</param>
        ''' <param name="exception">例外</param>
        Public Shared Sub IsNothing(ByVal o As Object, ByVal exception As Exception)
            If o Is Nothing Then
                Throw exception
            End If
        End Sub

#End Region
    End Class

End Namespace