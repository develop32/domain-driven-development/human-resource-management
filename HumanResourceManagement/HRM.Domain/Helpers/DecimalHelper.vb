﻿Imports System.Runtime.CompilerServices

Namespace Helpers

    Module DecimalHelper
        ''' <summary>
        ''' 少数点以下を指定桁数で四捨五入します。
        ''' </summary>
        ''' <param name="value">対象の値</param>
        ''' <param name="decimalPoint">四捨五入する桁数</param>
        ''' <returns>指定桁数で四捨五入した値</returns>
        ''' <example>
        '''  1. RoundString(12.345D, 2) ⇒ 12.34
        '''  2. 12.345D.RoundString(2) ⇒ 12.34
        ''' </example>
        <Extension()>
        Public Function RoundString(ByVal value As Decimal, ByVal decimalPoint As Integer) As String
            Dim temp As Decimal = Convert.ToSingle(Math.Round(value, decimalPoint))
            Return temp.ToString("F" & decimalPoint)
        End Function
    End Module

End Namespace