﻿Imports HRM.Domain.ValueObjects

Namespace Entities

    Public Class EmployeeEntity
        Public ReadOnly Property EmployeeId As Integer
        Public ReadOnly Property FirstName As String
        Public ReadOnly Property LastName As String
        Public ReadOnly Property Email As Email
        Public ReadOnly Property PhoneNumber As String
        Public ReadOnly Property HireDate As DateTime
        Public ReadOnly Property JobId As String
        Public ReadOnly Property JobTitle As String
        Public ReadOnly Property Salary As Salary
        Public ReadOnly Property CommissionPCT As CommissionPCT
        Public ReadOnly Property ManagerId As ManagerId
        Public ReadOnly Property DepartmentId As DepartmentId
        Public ReadOnly Property DepartmentName As String

        Public ReadOnly Property FullName As String
            Get
                Return FirstName & " " & LastName
            End Get
        End Property

        Public Sub New(employeeId As Integer,
                       firstName As String,
                       lastName As String,
                       email As String,
                       phoneNumber As String,
                       hireDate As DateTime,
                       jobId As String,
                       jobTitle As String,
                       salary As Decimal?,
                       commissionPCT As Decimal?,
                       managerId As Integer?,
                       departmentId As Integer?,
                       departmentName As String)
            Me.EmployeeId = employeeId
            Me.FirstName = firstName
            Me.LastName = lastName
            Me.Email = New Email(email)
            Me.PhoneNumber = phoneNumber
            Me.HireDate = hireDate
            Me.JobId = jobId
            Me.JobTitle = jobTitle
            Me.Salary = New Salary(salary)
            Me.CommissionPCT = New CommissionPCT(commissionPCT)
            Me.ManagerId = New ManagerId(managerId)
            Me.DepartmentId = New DepartmentId(departmentId)
            Me.DepartmentName = departmentName
        End Sub
    End Class

End Namespace