﻿Namespace Entities

    Public Class DepartmentEntity
        Public ReadOnly Property DepartmentId As Integer
        Public ReadOnly Property DepartmentName As String
        Public ReadOnly Property ManagerId As Nullable(Of Integer)
        Public ReadOnly Property LocationId As Integer

        Public Sub New(departmentId As Integer,
                       departmentName As String,
                       managerId As Nullable(Of Integer),
                       locationId As Integer)
            Me.DepartmentId = departmentId
            Me.DepartmentName = departmentName
            Me.ManagerId = managerId
            Me.LocationId = locationId
        End Sub
    End Class

End Namespace