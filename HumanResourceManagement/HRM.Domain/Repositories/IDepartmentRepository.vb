﻿Imports HRM.Domain.Entities

Namespace Repositories

    Public Interface IDepartmentRepository
        Function GetDepartments() As IReadOnlyList(Of DepartmentEntity)
    End Interface

End Namespace