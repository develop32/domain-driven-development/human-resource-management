﻿Imports HRM.Domain.Entities

Namespace Repositories

    Public Interface IEmployeeRepository
        Function GetEmployees(departmentId As Nullable(Of Integer), employeeId As Nullable(Of Integer)) As IReadOnlyList(Of EmployeeEntity)
    End Interface

End Namespace