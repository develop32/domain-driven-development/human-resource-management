﻿Imports System.Runtime.CompilerServices
Imports Oracle.ManagedDataAccess.Client

Friend Module OracleDataReaderHelper
    ''' <summary>
    ''' 指定カラムのデータを数値に変換した値を返します。
    ''' </summary>
    ''' <param name="reader">OracleDataReader</param>
    ''' <param name="columnName">カラム名</param>
    <Extension>
    Friend Function ToInteger(reader As OracleDataReader, columnName As String) As Integer
        Return Convert.ToInt32(reader(columnName))
    End Function

    ''' <summary>
    ''' 指定カラムのデータを数値に変換した値を返します。
    ''' 尚、Nullの場合はNothingを返します。
    ''' </summary>
    ''' <param name="reader">OracleDataReader</param>
    ''' <param name="columnName">カラム名</param>
    <Extension>
    Friend Function ToIntegerOrNothing(reader As OracleDataReader, columnName As String) As Integer?
        If IsDBNull(reader(columnName)) Then
            Return Nothing
        Else
            Return Convert.ToInt32(reader(columnName))
        End If
    End Function

    ''' <summary>
    ''' 指定カラムのデータを少数に変換した値を返します。
    ''' </summary>
    ''' <param name="reader">OracleDataReader</param>
    ''' <param name="columnName">カラム名</param>
    <Extension>
    Friend Function ToDecimal(reader As OracleDataReader, columnName As String) As Decimal
        Return Convert.ToDecimal(reader(columnName))
    End Function

    ''' <summary>
    ''' 指定カラムのデータを少数に変換した値を返します。
    ''' 尚、Nullの場合はNothingを返します。
    ''' </summary>
    ''' <param name="reader">OracleDataReader</param>
    ''' <param name="columnName">カラム名</param>
    <Extension>
    Friend Function ToDecimalOrNothing(reader As OracleDataReader, columnName As String) As Decimal?
        If IsDBNull(reader(columnName)) Then
            Return Nothing
        Else
            Return Convert.ToDecimal(reader(columnName))
        End If
    End Function

End Module
