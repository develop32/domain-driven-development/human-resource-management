﻿Imports System.Text
Imports HRM.Domain.Entities
Imports HRM.Domain.Repositories
Imports Oracle.ManagedDataAccess.Client

Namespace Oracle

    Public Class EmployeesOracle
        Implements IEmployeeRepository

        Public Function GetEmployees(departmentId As Integer?, employeeId As Integer?) As IReadOnlyList(Of EmployeeEntity) Implements IEmployeeRepository.GetEmployees
            Dim sql As New StringBuilder
            sql.AppendLine("SELECT")
            sql.AppendLine("    EMP.EMPLOYEE_ID, ")
            sql.AppendLine("    EMP.FIRST_NAME, ")
            sql.AppendLine("    EMP.LAST_NAME, ")
            sql.AppendLine("    EMP.EMAIL, ")
            sql.AppendLine("    EMP.PHONE_NUMBER, ")
            sql.AppendLine("    EMP.HIRE_DATE, ")
            sql.AppendLine("    EMP.JOB_ID, ")
            sql.AppendLine("    JOB.JOB_TITLE, ")
            sql.AppendLine("    EMP.SALARY, ")
            sql.AppendLine("    EMP.COMMISSION_PCT, ")
            sql.AppendLine("    EMP.MANAGER_ID, ")
            sql.AppendLine("    EMP.DEPARTMENT_ID, ")
            sql.AppendLine("    NVL(DPT.DEPARTMENT_NAME, '-') AS DEPARTMENT_NAME")
            sql.AppendLine("FROM")
            sql.AppendLine("    EMPLOYEES EMP ")
            sql.AppendLine("    LEFT OUTER JOIN DEPARTMENTS DPT ")
            sql.AppendLine("        ON EMP.DEPARTMENT_ID = DPT.DEPARTMENT_ID ")
            sql.AppendLine("    INNER JOIN JOBS JOB ")
            sql.AppendLine("        ON EMP.JOB_ID = JOB.JOB_ID ")
            sql.AppendLine("WHERE")
            sql.AppendLine("    ( ")
            sql.AppendLine("        :DEPARTMENT_ID IS NULL ")
            sql.AppendLine("        OR EMP.DEPARTMENT_ID = :DEPARTMENT_ID")
            sql.AppendLine("    ) ")
            sql.AppendLine("    AND ( ")
            sql.AppendLine("        :EMPLOYEE_ID IS NULL ")
            sql.AppendLine("        OR EMP.EMPLOYEE_ID = :EMPLOYEE_ID")
            sql.AppendLine("    )")
            sql.AppendLine("ORDER BY")
            sql.AppendLine("    EMP.EMPLOYEE_ID")


            Dim parameters = New List(Of OracleParameter) From {
                New OracleParameter(":DEPARTMENT_ID", departmentId),
                New OracleParameter(":EMPLOYEE_ID", employeeId)
            }


            Return OracleHelper.Query(
                sql.ToString,
                parameters.ToArray,
                Function(reader) New EmployeeEntity(Convert.ToInt32(reader("EMPLOYEE_ID")),
                                                    Convert.ToString(reader("FIRST_NAME")),
                                                    Convert.ToString(reader("LAST_NAME")),
                                                    Convert.ToString(reader("EMAIL")),
                                                    Convert.ToString(reader("PHONE_NUMBER")),
                                                    Convert.ToDateTime(reader("HIRE_DATE")),
                                                    Convert.ToString(reader("JOB_ID")),
                                                    Convert.ToString(reader("JOB_TITLE")),
                                                    reader.ToDecimalOrNothing("SALARY"),
                                                    reader.ToDecimalOrNothing("COMMISSION_PCT"),
                                                    reader.ToIntegerOrNothing("MANAGER_ID"),
                                                    reader.ToIntegerOrNothing("DEPARTMENT_ID"),
                                                    Convert.ToString(reader("DEPARTMENT_NAME"))))
        End Function
    End Class

End Namespace