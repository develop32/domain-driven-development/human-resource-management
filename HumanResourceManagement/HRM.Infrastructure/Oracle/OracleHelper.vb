﻿Imports System.Configuration
Imports Oracle.ManagedDataAccess.Client

Namespace Oracle

    ''' <summary>
    ''' オラクルヘルパークラス
    ''' </summary>
    Friend Class OracleHelper

#Region "フィールド"
        ''' <summary>接続文字列</summary>
        Friend Shared ConnectionString As String = ConfigurationManager.AppSettings("ConnectionString")
#End Region

#Region "メソッド"
        ''' <summary>
        ''' クエリを実行します。
        ''' </summary>
        ''' <typeparam name="T">返却してほしいエンティティ</typeparam>
        ''' <param name="sql">SELECT文</param>
        ''' <param name="createEntity">取得したデータをエンティティに設定する式</param>
        ''' <returns>返却してほしいエンティティのリスト</returns>
        Friend Shared Function Query(Of T)(ByVal sql As String, ByVal createEntity As Func(Of OracleDataReader, T)) As IReadOnlyList(Of T)
            Return Query(Of T)(sql, Nothing, createEntity)
        End Function

        ''' <summary>
        ''' クエリを実行します。
        ''' </summary>
        ''' <typeparam name="T">返却してほしいエンティティ</typeparam>
        ''' <param name="sql">SELECT文</param>
        ''' <param name="parameters">パラメータ</param>
        ''' <param name="createEntity">取得したデータをエンティティに設定する式</param>
        ''' <returns>返却してほしいエンティティのリスト</returns>
        Friend Shared Function Query(Of T)(ByVal sql As String, ByVal parameters() As OracleParameter, ByVal createEntity As Func(Of OracleDataReader, T)) As IReadOnlyList(Of T)
            Dim result = New List(Of T)()

            Using connection = New OracleConnection(ConnectionString)

                Using command = New OracleCommand(sql, connection)
                    connection.Open()

                    If parameters IsNot Nothing Then
                        command.Parameters.AddRange(parameters)
                        command.BindByName = True
                    End If

                    Using reader = command.ExecuteReader()

                        While reader.Read()
                            result.Add(createEntity(reader))
                        End While
                    End Using
                End Using
            End Using

            Return result
        End Function

        ''' <summary>
        ''' クエリを実行します。(取得結果1件)
        ''' </summary>
        ''' <typeparam name="T">返却してほしいエンティティ</typeparam>
        ''' <param name="sql">SELECT文</param>
        ''' <param name="createEntity">取得したデータをエンティティに設定する式</param>
        ''' <param name="nullEntity">データなしの時に返却してほしいもの</param>
        ''' <returns>エンティティ</returns>
        Friend Shared Function QuerySingle(Of T)(ByVal sql As String, ByVal createEntity As Func(Of OracleDataReader, T), ByVal nullEntity As T) As T
            Return QuerySingle(Of T)(sql, Nothing, createEntity, nullEntity)
        End Function

        ''' <summary>
        ''' クエリを実行します。(取得結果1件)
        ''' </summary>
        ''' <typeparam name="T">返却してほしいエンティティ</typeparam>
        ''' <param name="sql">SELECT文</param>
        ''' <param name="parameters">パラメータ</param>
        ''' <param name="createEntity">取得したデータをエンティティに設定する式</param>
        ''' <param name="nullEntity">データなしの時に返却してほしいもの</param>
        ''' <returns>エンティティ</returns>
        Friend Shared Function QuerySingle(Of T)(ByVal sql As String, ByVal parameters As OracleParameter(), ByVal createEntity As Func(Of OracleDataReader, T), ByVal nullEntity As T) As T
            Using connection = New OracleConnection(ConnectionString)

                Using command = New OracleCommand(sql, connection)
                    connection.Open()

                    If parameters IsNot Nothing Then
                        command.Parameters.AddRange(parameters)
                        command.BindByName = True
                    End If

                    Using reader = command.ExecuteReader()

                        While reader.Read()
                            Return createEntity(reader)
                        End While
                    End Using
                End Using
            End Using

            Return nullEntity
        End Function

        ''' <summary>
        ''' SQLを実行します。
        ''' </summary>
        ''' <param name="insert">INSERT文</param>
        ''' <param name="update">UPDATE文</param>
        ''' <param name="parameters">パラメータ</param>
        Friend Shared Sub Execute(ByVal insert As String, ByVal update As String, ByVal parameters As OracleParameter())
            Using connection = New OracleConnection(ConnectionString)

                Using command = New OracleCommand(update, connection)
                    connection.Open()

                    If parameters IsNot Nothing Then
                        command.Parameters.AddRange(parameters)
                        command.BindByName = True
                    End If

                    If command.ExecuteNonQuery() < 1 Then
                        command.CommandText = insert
                        command.ExecuteNonQuery()
                    End If
                End Using
            End Using
        End Sub

        ''' <summary>
        ''' SQLを実行します。
        ''' </summary>
        ''' <param name="sql">SQL文</param>
        ''' <param name="parameters">パラメータ</param>
        Friend Shared Sub Execute(ByVal sql As String, ByVal parameters As OracleParameter())
            Using connection = New OracleConnection(ConnectionString)

                Using command = New OracleCommand(sql, connection)
                    connection.Open()

                    If parameters IsNot Nothing Then
                        command.Parameters.AddRange(parameters)
                        command.BindByName = True
                    End If

                    command.ExecuteNonQuery()
                End Using
            End Using
        End Sub
#End Region
    End Class

End Namespace