﻿Imports System.Text
Imports HRM.Domain.Entities
Imports HRM.Domain.Repositories

Namespace Oracle

    Public Class DepartmentsOracle
        Implements IDepartmentRepository

        Public Function GetDepartments() As IReadOnlyList(Of DepartmentEntity) Implements IDepartmentRepository.GetDepartments
            Dim sql As New StringBuilder
            sql.AppendLine("SELECT ")
            sql.AppendLine("    DEPARTMENT_ID,   -- 部門ID ")
            sql.AppendLine("    DEPARTMENT_NAME, -- 部門名 ")
            sql.AppendLine("    MANAGER_ID,      -- 部門管理者ID ")
            sql.AppendLine("    LOCATION_ID      -- 所在地ID ")
            sql.AppendLine("FROM ")
            sql.AppendLine("    DEPARTMENTS ")
            sql.AppendLine("ORDER BY ")
            sql.AppendLine("    DEPARTMENT_ID ")

            Return OracleHelper.Query(sql.ToString,
                Function(reader) New DepartmentEntity(Convert.ToInt32(reader("DEPARTMENT_ID")),
                                                      Convert.ToString(reader("DEPARTMENT_NAME")),
                                                      If(reader("MANAGER_ID") Is DBNull.Value, Nothing, Convert.ToInt32(reader("MANAGER_ID"))),
                                                      Convert.ToInt32(reader("LOCATION_ID"))))
        End Function
    End Class

End Namespace