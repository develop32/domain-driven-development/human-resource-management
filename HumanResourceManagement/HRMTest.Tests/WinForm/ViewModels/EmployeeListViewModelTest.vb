﻿Imports System.Text
Imports HRM.Domain.Entities
Imports HRM.Domain.Exceptions
Imports HRM.Domain.Exceptions.AbstractExceptionBase
Imports HRM.Domain.Repositories
Imports HRM.WinForm.ViewModels
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports Moq


' テストリスト
' done: 初期画面
'   done: 部署コンボボックスが空であること
'   done: 部署コンボボックスの選択可能項目が3つであること
'   done: 従業員IDが空文字であること
'   done: 一覧の件数が0件であること
' todo: 検索ボタン押下時
'   done: 従業員IDがDBに存在する従業員ID100の場合
'       done: 一覧の件数が1件であること
'       done: 一覧の従業員IDが100であること
'       done: 一覧のフルネームがStevenｽﾍﾟｰｽKingであること
'       done: 一覧のEmailがSKINGｱｯﾄoracleﾄﾞｯﾄcomであること
'       done: 一覧の電話番号が515ﾄﾞｯﾄ123ﾄﾞｯﾄ4567であること
'       done: 一覧の入社日が2003ｽﾗ6ｽﾗ17ｽﾍﾟｰｽ00ｺﾛﾝ00ｺﾛﾝ00であること
'       done: 一覧の職種がPresidentであること
'       done: 一覧の月俸が24000ﾄﾞｯﾄ00ﾄﾞﾙであること
'       done: 一覧の歩合率がﾊｲﾌﾝであること
'       done: 一覧の管理者IDがﾊｲﾌﾝであること
'       done: 一覧の部門がExecutiveであること
'   done: 従業員IDがDBに存在しない従業員ID999999の場合
'       done: データなし情報例外が発生しデータがありませんメッセージが表示され一覧が0件であること
'   todo: 部門が90_Executive場合
'       todo: 一覧の件数が2件であること




<TestClass()> Public Class EmployeeListViewModelTest
    Public Class TestData
        Public Shared ReadOnly Property Departments As New List(Of DepartmentEntity) From {
                New DepartmentEntity(30, "Purchasing", 114, 1700),
                New DepartmentEntity(60, "IT", 103, 1400),
                New DepartmentEntity(90, "Executive", 100, 1700)
            }
        Public Shared ReadOnly Property Employee_Nothing_100 As New List(Of EmployeeEntity) From {
                New EmployeeEntity(100, "Steven", "King", "SKING", "515.123.4567",
                                   "2003 / 6 / 17 0: 00:00", "AD_PRES", "President",
                                   24000.0, Nothing, Nothing, 90, "Executive")
            }
        Public Shared ReadOnly Property Employee_90_Nothign As New List(Of EmployeeEntity) From {
                New EmployeeEntity(100, "Steven", "King", "SKING", "515.123.4567",
                                   "2003 / 6 / 17 0: 00:00", "AD_PRES", "President",
                                   24000.0, Nothing, Nothing, 90, "Executive"),
                New EmployeeEntity(101, "Neena", "Kochhar", "NKOCHHAR", "515.123.4568",
                                   "2005/09/21 0:00:00", "AD_VP", "Administration Vice President",
                                   17000.0, Nothing, 100, 90, "Executive")
            }
    End Class

    <TestClass()> Public Class 初期画面
        Private _target As EmployeeListViewModel

        <TestInitialize> Public Sub 前準備()
            Dim departmentMock As New Mock(Of IDepartmentRepository)
            departmentMock.Setup(Function(x) x.GetDepartments()).Returns(TestData.Departments)

            Dim employeeMock As New Mock(Of IEmployeeRepository)
            employeeMock.Setup(Function(x) x.GetEmployees(Nothing, 1)).Returns(TestData.Employee_Nothing_100)

            _target = New EmployeeListViewModel(departmentMock.Object, employeeMock.Object)
        End Sub

        <TestMethod()> Public Sub 従業員IDが空文字であること()
            Assert.AreEqual("", _target.EmployeeIdText)
        End Sub

        <TestMethod> Public Sub 部署コンボボックスが空であること()
            Assert.IsNull(_target.SelectedDepartmentId)
        End Sub

        <TestMethod> Public Sub 部署コンボボックスの選択可能項目が3つであること()
            Assert.AreEqual(3, _target.Departments.Count)
        End Sub

        <TestMethod()> Public Sub 一覧の件数が0件であること()
            Assert.AreEqual(0, _target.Employees.Count)
        End Sub
    End Class

    <TestClass()> Public Class 検索ボタン押下
        <TestClass()> Public Class 従業員IDがDBに存在する従業員ID100の場合
            Private _target As EmployeeListViewModel

            <TestInitialize> Public Sub 前準備()
                Dim departmentMock As New Mock(Of IDepartmentRepository)
                departmentMock.Setup(Function(x) x.GetDepartments()).Returns(TestData.Departments)
                Dim employeeMock As New Mock(Of IEmployeeRepository)
                employeeMock.Setup(Function(x) x.GetEmployees(Nothing, 100)).Returns(TestData.Employee_Nothing_100)

                _target = New EmployeeListViewModel(departmentMock.Object, employeeMock.Object) With {
                    .EmployeeIdText = "100"
                }
                _target.Search()
            End Sub

            <TestMethod> Public Sub 一覧の件数が1件であること()
                Assert.AreEqual(1, _target.Employees.Count)
            End Sub

            <TestMethod> Public Sub 一覧の従業員IDが100であること()
                Assert.AreEqual("100", _target.Employees(0).EmployeeIdText)
            End Sub
            <TestMethod> Public Sub 一覧のフルネームがStevenｽﾍﾟｰｽKingであること()
                Assert.AreEqual("Steven King", _target.Employees(0).FullNameText)
            End Sub
            <TestMethod> Public Sub 一覧のEmailがSKINGｱｯﾄoracleﾄﾞｯﾄcomであること()
                Assert.AreEqual("SKING@oracle.com", _target.Employees(0).EmailText)
            End Sub
            <TestMethod> Public Sub 一覧の電話番号が515ﾄﾞｯﾄ123ﾄﾞｯﾄ4567であること()
                Assert.AreEqual("515.123.4567", _target.Employees(0).PhoneNumberText)
            End Sub
            <TestMethod> Public Sub 一覧の入社日が2003ｽﾗ6ｽﾗ17ｽﾍﾟｰｽ00ｺﾛﾝ00ｺﾛﾝ00であること()
                Assert.AreEqual("2003/06/17 0:00:00", _target.Employees(0).HireDateText)
            End Sub
            <TestMethod> Public Sub 一覧の職種がPresidentであること()
                Assert.AreEqual("President", _target.Employees(0).JobTitleText)
            End Sub
            <TestMethod> Public Sub 一覧の月俸が24000ﾄﾞｯﾄ00ﾄﾞﾙであること()
                Assert.AreEqual("24000.00$", _target.Employees(0).SalaryText)
            End Sub
            <TestMethod> Public Sub 一覧の歩合率がﾊｲﾌﾝであること()
                Assert.AreEqual("-", _target.Employees(0).CommissionPCTText)
            End Sub
            <TestMethod> Public Sub 一覧の管理者IDがﾊｲﾌﾝであること()
                Assert.AreEqual("-", _target.Employees(0).ManagerIdText)
            End Sub
            <TestMethod> Public Sub 一覧の部門がExecutiveであること()
                Assert.AreEqual("Executive", _target.Employees(0).DepartmentNameText)
            End Sub

        End Class

        <TestClass()> Public Class 従業員IDがDBに存在しない従業員ID999999の場合
            Private _target As EmployeeListViewModel

            <TestInitialize> Public Sub 前準備()
                Dim departmentMock As New Mock(Of IDepartmentRepository)
                departmentMock.Setup(Function(x) x.GetDepartments()).Returns(TestData.Departments)
                Dim employeeMock As New Mock(Of IEmployeeRepository)

                _target = New EmployeeListViewModel(departmentMock.Object, employeeMock.Object) With {
                    .EmployeeIdText = "999999"
                }
            End Sub

            <TestMethod> Public Sub データなし情報例外が発生しデータがありませんメッセージが表示され一覧が0件であること()
                Dim ex As DataNotFoundException = Assert.ThrowsException(Of DataNotFoundException)(Sub() _target.Search())
                Assert.AreEqual(ExceptionKind.Info, ex.Kind)
                Assert.AreEqual("データがありません。", ex.Message)
                Assert.AreEqual(0, _target.Employees.Count)
            End Sub
        End Class

        <TestClass()> Public Class 部門が90_Executive場合
            Private _target As EmployeeListViewModel

            <TestInitialize> Public Sub 前準備()
                Dim departmentMock As New Mock(Of IDepartmentRepository)
                departmentMock.Setup(Function(x) x.GetDepartments()).Returns(TestData.Departments)
                Dim employeeMock As New Mock(Of IEmployeeRepository)
                employeeMock.Setup(Function(x) x.GetEmployees(90, Nothing)).Returns(TestData.Employee_90_Nothign)

                _target = New EmployeeListViewModel(departmentMock.Object, employeeMock.Object) With {
                    .SelectedDepartmentId = 90,
                    .EmployeeIdText = ""
                }
                _target.Search()
            End Sub

            <TestMethod> Public Sub 一覧の件数が2件であること()
                Assert.AreEqual(2, _target.Employees.Count)
            End Sub
        End Class


    End Class
End Class