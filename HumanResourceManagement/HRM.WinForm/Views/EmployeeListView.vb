﻿Imports HRM.Domain.Entities
Imports HRM.WinForm.ViewModels

Public Class EmployeeListView
    Private _viewModel As New EmployeeListViewModel

    Public Sub New()

        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        DepartmentsComboBox.DropDownStyle = ComboBoxStyle.DropDownList
        DepartmentsComboBox.DataBindings.Add("SelectedValue", _viewModel,
                                                NameOf(_viewModel.SelectedDepartmentId))
        DepartmentsComboBox.DataBindings.Add("DataSource", _viewModel,
                                                NameOf(_viewModel.Departments))
        DepartmentsComboBox.ValueMember = NameOf(DepartmentEntity.DepartmentId)
        DepartmentsComboBox.DisplayMember = NameOf(DepartmentEntity.DepartmentName)

        EmployeeIdTextBox.DataBindings.Add("Text", _viewModel, NameOf(_viewModel.EmployeeIdText))

        EmployeesDataGridView.DataSource = _viewModel.Employees
        EmployeesDataGridView.Columns(1).Frozen = True
        SetColumns()
    End Sub

    Private Sub SetColumns()
        SetColumn("EmployeeIdText", "従業員ID")
        SetColumn("FullNameText", "フルネーム")
        SetColumn("EmailText", "Eメール")
        SetColumn("PhoneNumberText", "電話番号")
        SetColumn("HireDateText", "入社日")
        SetColumn("JobTitleText", "職種")
        SetColumn("SalaryText", "月俸")
        SetColumn("CommissionPCTText", "歩合率")
        SetColumn("ManagerIdText", "管理者ID")
        SetColumn("DepartmentNameText", "部署名")
    End Sub

    Private Sub SetColumn(name As String, head As String)
        EmployeesDataGridView.Columns(name).HeaderText = head
    End Sub

    Private Sub SearchButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
        _viewModel.Search()
    End Sub

    Private Sub EmployeesDataGridView_DoubleClick(sender As Object, e As EventArgs) Handles EmployeesDataGridView.DoubleClick
        ' todo: 保存画面遷移
        Dim selectedEmployeeId As Integer
        selectedEmployeeId = Convert.ToInt16(EmployeesDataGridView(0, EmployeesDataGridView.CurrentRow.Index).Value)
        MessageBox.Show(selectedEmployeeId,
                        "Todo!!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error)
    End Sub
End Class