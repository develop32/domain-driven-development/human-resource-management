﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmployeeListView
    Inherits AbstractBaseView

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.SearchButton = New System.Windows.Forms.Button()
        Me.EmployeeIdTextBox = New System.Windows.Forms.TextBox()
        Me.DepartmentsComboBox = New System.Windows.Forms.ComboBox()
        Me.EmployeeIdLabel = New System.Windows.Forms.Label()
        Me.DepartmentLabel = New System.Windows.Forms.Label()
        Me.EmployeesDataGridView = New System.Windows.Forms.DataGridView()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.EmployeesDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SearchButton)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EmployeeIdTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.DepartmentsComboBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EmployeeIdLabel)
        Me.SplitContainer1.Panel1.Controls.Add(Me.DepartmentLabel)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.EmployeesDataGridView)
        Me.SplitContainer1.Size = New System.Drawing.Size(955, 428)
        Me.SplitContainer1.SplitterDistance = 54
        Me.SplitContainer1.TabIndex = 1
        '
        'SearchButton
        '
        Me.SearchButton.Location = New System.Drawing.Point(433, 18)
        Me.SearchButton.Name = "SearchButton"
        Me.SearchButton.Size = New System.Drawing.Size(75, 23)
        Me.SearchButton.TabIndex = 3
        Me.SearchButton.Text = "検索"
        Me.SearchButton.UseVisualStyleBackColor = True
        '
        'EmployeeIdTextBox
        '
        Me.EmployeeIdTextBox.Location = New System.Drawing.Point(301, 18)
        Me.EmployeeIdTextBox.Name = "EmployeeIdTextBox"
        Me.EmployeeIdTextBox.Size = New System.Drawing.Size(100, 24)
        Me.EmployeeIdTextBox.TabIndex = 2
        '
        'DepartmentsComboBox
        '
        Me.DepartmentsComboBox.FormattingEnabled = True
        Me.DepartmentsComboBox.Location = New System.Drawing.Point(65, 18)
        Me.DepartmentsComboBox.Name = "DepartmentsComboBox"
        Me.DepartmentsComboBox.Size = New System.Drawing.Size(121, 25)
        Me.DepartmentsComboBox.TabIndex = 0
        '
        'EmployeeIdLabel
        '
        Me.EmployeeIdLabel.AutoSize = True
        Me.EmployeeIdLabel.Location = New System.Drawing.Point(220, 21)
        Me.EmployeeIdLabel.Name = "EmployeeIdLabel"
        Me.EmployeeIdLabel.Size = New System.Drawing.Size(75, 17)
        Me.EmployeeIdLabel.TabIndex = 1
        Me.EmployeeIdLabel.Text = "従業員ID："
        '
        'DepartmentLabel
        '
        Me.DepartmentLabel.AutoSize = True
        Me.DepartmentLabel.Location = New System.Drawing.Point(12, 21)
        Me.DepartmentLabel.Name = "DepartmentLabel"
        Me.DepartmentLabel.Size = New System.Drawing.Size(47, 17)
        Me.DepartmentLabel.TabIndex = 1
        Me.DepartmentLabel.Text = "部門："
        '
        'EmployeesDataGridView
        '
        Me.EmployeesDataGridView.AllowUserToAddRows = False
        Me.EmployeesDataGridView.AllowUserToDeleteRows = False
        Me.EmployeesDataGridView.AllowUserToOrderColumns = True
        Me.EmployeesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.EmployeesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.EmployeesDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.EmployeesDataGridView.MultiSelect = False
        Me.EmployeesDataGridView.Name = "EmployeesDataGridView"
        Me.EmployeesDataGridView.ReadOnly = True
        Me.EmployeesDataGridView.RowTemplate.Height = 21
        Me.EmployeesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.EmployeesDataGridView.Size = New System.Drawing.Size(955, 370)
        Me.EmployeesDataGridView.TabIndex = 0
        '
        'EmployeeListView
        '
        Me.AcceptButton = Me.SearchButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(955, 450)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "EmployeeListView"
        Me.Text = "EmployeeListView"
        Me.Controls.SetChildIndex(Me.SplitContainer1, 0)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.EmployeesDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents DepartmentLabel As Label
    Friend WithEvents DepartmentsComboBox As ComboBox
    Friend WithEvents EmployeeIdTextBox As TextBox
    Friend WithEvents EmployeeIdLabel As Label
    Friend WithEvents SearchButton As Button
    Friend WithEvents EmployeesDataGridView As DataGridView
End Class
