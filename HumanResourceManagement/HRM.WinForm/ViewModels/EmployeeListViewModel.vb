﻿Imports System.ComponentModel
Imports HRM.Domain.Entities
Imports HRM.Domain.Exceptions
Imports HRM.Domain.Helpers
Imports HRM.Domain.Repositories
Imports HRM.Infrastructure.Oracle

Namespace ViewModels

    Public Class EmployeeListViewModel
        Inherits AbstractViewModelBase

        Private ReadOnly _departmentRepository As IDepartmentRepository
        Private ReadOnly _employeeRepository As IEmployeeRepository


        Public Property EmployeeIdText As String = String.Empty
        Public Property SelectedDepartmentId As Object
        Public Property Employees As New BindingList(Of EmployeeListViewModelEmployee)
        Public Property Departments As New BindingList(Of DepartmentEntity)

        Public Sub New()
            Me.New(New DepartmentsOracle, New EmployeesOracle)
        End Sub

        Public Sub New(departmentRepository As IDepartmentRepository,
                       employeeRepository As IEmployeeRepository)
            _departmentRepository = departmentRepository
            _employeeRepository = employeeRepository

            For Each department In _departmentRepository.GetDepartments
                Departments.Add(New DepartmentEntity(department.DepartmentId,
                                                     department.DepartmentName,
                                                     department.ManagerId,
                                                     department.LocationId))
            Next
        End Sub

        Public Sub Search()
            'Me.Employees = New BindingList(Of EmployeeListViewModelEmployee)
            Me.Employees.Clear()

            Dim employeeId As Nullable(Of Integer)
            If EmployeeIdText = String.Empty Then
                employeeId = Nothing
            Else
                employeeId = EmployeeIdText
            End If

            Dim employees As IReadOnlyList(Of EmployeeEntity) = _employeeRepository.GetEmployees(SelectedDepartmentId, employeeId)

            Guard.IsNothing(employees, New DataNotFoundException("データがありません。"))

            For Each employee As EmployeeEntity In employees
                Me.Employees.Add(New EmployeeListViewModelEmployee(employee))
            Next
        End Sub
    End Class

End Namespace