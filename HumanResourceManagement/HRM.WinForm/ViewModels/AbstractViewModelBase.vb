﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Namespace ViewModels
    ''' <summary>
    ''' ViewModel抽象クラス
    ''' </summary>
    Public MustInherit Class AbstractViewModelBase
        Implements INotifyPropertyChanged

#Region "メソッド"
        ''' <summary>
        ''' プロパティを設定します。
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="field">設定前の値</param>
        ''' <param name="value">設定したい値</param>
        ''' <param name="propertyName">プロパティ名</param>
        ''' <returns></returns>
        Protected Function SetProperty(Of T)(ByRef field As T,
                                             ByVal value As T,
                                             <CallerMemberName> ByVal Optional propertyName As String = Nothing) As Boolean
            If Equals(field, value) Then
                Return False
            End If

            field = value
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))

            Return True
        End Function

        ''' <summary>
        ''' システム日付を取得します。
        ''' </summary>
        ''' <returns>システム日付</returns>
        Public Overridable Function GetDateTime() As DateTime
            Return Now
        End Function
#End Region

#Region "イベント"
        ''' <summary>
        ''' プロパティチェンジイベント
        ''' </summary>
        Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
#End Region

    End Class

End Namespace