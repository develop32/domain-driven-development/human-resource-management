﻿Imports HRM.Domain.Entities

Namespace ViewModels
    Public Class EmployeeListViewModelEmployee
        Private ReadOnly _entity As EmployeeEntity

        Public ReadOnly Property EmployeeIdText As String
            Get
                Return _entity.EmployeeId.ToString
            End Get
        End Property
        Public ReadOnly Property FullNameText As String
            Get
                Return _entity.FullName
            End Get
        End Property
        Public ReadOnly Property EmailText As String
            Get
                Return _entity.Email.DisplayValue
            End Get
        End Property
        Public ReadOnly Property PhoneNumberText As String
            Get
                Return _entity.PhoneNumber
            End Get
        End Property
        Public ReadOnly Property HireDateText As String
            Get
                Return _entity.HireDate.ToString
            End Get
        End Property
        Public ReadOnly Property JobTitleText As String
            Get
                Return _entity.JobTitle
            End Get
        End Property
        Public ReadOnly Property SalaryText As String
            Get
                Return _entity.Salary.DisplayValue
            End Get
        End Property
        Public ReadOnly Property CommissionPCTText As String
            Get
                Return _entity.CommissionPCT.DisplayValue
            End Get
        End Property
        Public ReadOnly Property ManagerIdText As String
            Get
                Return _entity.ManagerId.DisplayValue
            End Get
        End Property
        Public ReadOnly Property DepartmentNameText As String
            Get
                Return _entity.DepartmentName
            End Get
        End Property

        Public Sub New(entity As EmployeeEntity)
            _entity = entity
        End Sub
    End Class
End Namespace
